package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Ball extends Sprite
	{
		[Embed(source="../bin/images/ball.png")]
		private var embededImage : Class;
		
		public const GRAVITY : Number = 4.8;
		
		public var velocity : Point;
		
		public function Ball() 
		{
			setup();
		}
		
		private function setup() : void
		{
			var image : Bitmap = new embededImage();
			
			image.smoothing = true;
			
			image.x = - image.width / 2;
			image.y = - image.height / 2;
			
			velocity = new Point();
			
			this.addChild( image );
		}
		
		public function tick():void
		{
			
			velocity.y += GRAVITY;
			
			if (y > stage.stageHeight - height / 2)
			{
				velocity.y *= -0.6;
				y = stage.stageHeight - height / 2;
				scaleX *= 0.75;
				scaleY *= 0.75;
			}
			
			if (Math.abs(velocity.y) < 0.5)
			{
				velocity.y = 0;
			}
			
			this.y += velocity.y;
		}
	}
}