package 
{
	import flash.automation.KeyboardAutomationAction;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Main extends Sprite 
	{
		private static const BALL_SPEED : int = 5;
		
		private var ball : Ball;
		
		private var downKeys : Dictionary;	// A look up for key code to a value indicating if the key is down or not (boolean)
		
		private var ballList : Vector.<Ball>;
		
		public function Main():void 
		{
			if ( stage ) init();
			else addEventListener( Event.ADDED_TO_STAGE, init );
		}
		
		private function init( e:Event = null ):void 
		{
			removeEventListener( Event.ADDED_TO_STAGE, init );
			// entry point
			
			// Create new ball
			ball = new Ball();
			
			ballList = new Vector.<Ball>();
			
			// Add the ball to our stage to display it
			this.addChild( ball );
			
			// Set the ball to the middle of the screen
			ball.x = ( this.stage.stageWidth / 2 ) - ( ball.width / 2 );		// Note that the top left corner of the screen is 0, 0 
			ball.y = ( this.stage.stageHeight / 2 ) - ( ball.height / 2 );		// and this is the same for the ball
			
			// Create a new dictionary for tracking if the keys are pressed
			downKeys = new Dictionary();
			
			// Setup listeners for when keys are pressed
			this.stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			this.stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			this.stage.addEventListener( Event.ENTER_FRAME, onEnterFrame );
			
			this.stage.addEventListener( MouseEvent.CLICK, onMouseClick);
		}
		
		private function onMouseClick( e:MouseEvent ):void
		{
			var newBall:Ball = new Ball();
			newBall.x = e.stageX;
			newBall.y = e.stageY;
			addChild(newBall);
			ballList.push(newBall);
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			downKeys[ e.keyCode ] = true;	// Indicate that this key is down (whatever the key is);
		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{
			downKeys[ e.keyCode ] = false;	// Indicate that this key is no longer down
		}
		
		private function onEnterFrame( e:Event ):void
		{
			if ( downKeys[ Keyboard.W ] == true )
			{
				// If W is pressed, move the ball up
				ball.y -= BALL_SPEED;	// Note that -y is the up direction
			}
			else if ( downKeys[ Keyboard.S ] == true )
			{
				// Otherwise, if we are pressing down, go down
				ball.y += BALL_SPEED;
			}
			
			if ( downKeys[ Keyboard.A ] == true )
			{
				ball.x -= BALL_SPEED;	// left is -x
			}
			else if ( downKeys[ Keyboard.D ] == true )
			{
				ball.x += BALL_SPEED;
			}
			
			for each (var currentBall:Ball in ballList)
			{
				currentBall.tick();
			}
			
		}
	}
}