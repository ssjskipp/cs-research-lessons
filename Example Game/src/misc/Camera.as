package misc
{
	import flash.display.DisplayObject;
	import interfaces.ITickable;
	
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 * 
	 * Contains the logic for the camera.
	 * 
	 * The camera is based about the center of the display area
	 */
	
	public class Camera implements ITickable {
		
		//Position of the camera
		public var x:Number;
		public var y:Number;
		
		//The target display object to change with respect to the camera
		public var target:DisplayObject;
		
		public var displayWidth:Number;
		public var displayHeight:Number;
		
		public function Camera()
		{
			setup();
		}
		
		private function setup():void
		{
			displayHeight = 400;
			displayWidth = 550;
		}
		
		public function update(delta:Number):void
		{
			//Basic camera, move the main display opposite. Also, make sure to account for display width.
			target.x = displayWidth / 2 - x;
			target.y = displayHeight / 2 - y;
		}
		
	}

}