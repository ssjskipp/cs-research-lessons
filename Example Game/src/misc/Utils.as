package misc {
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 */
	public class Utils {
		
		public function Utils() {
			
		}
		
		public static function Sign(number:Number):Number
		{
			if (number < 0) return -1;
			return 1;
		}
		
	}

}