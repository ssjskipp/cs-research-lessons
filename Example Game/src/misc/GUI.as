package misc
{
	import flash.display.Sprite;
	import interfaces.ITickable;
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 */
	public class GUI extends Sprite implements ITickable {
		
		public function GUI() {
			
		}
		
		public function update(delta:Number):void {
			
			// The GUI object is intended to stay fixed, outside the camera.
			
			// I'll draw a circle around the mouse for now
			
			graphics.clear();
			graphics.lineStyle(1, 0xFF0000);
			graphics.drawCircle(stage.mouseX, stage.mouseY, 5);
		}
		
	}

}