package misc {
	
	import actors.TerrainRect;
	import flash.display.Sprite;
	import flash.geom.Point;
	import physics.RectBody;
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 * 
	 * Represents the level you play on. Does not include background.
	 */
	public class Level extends Sprite {
		
		private var levelRects:Vector.<TerrainRect>;
		
		public function Level() {
			
			setup();
			
		}
		
		private function setup():void {
			levelRects = new Vector.<TerrainRect>();
			
			//Bottm, left, right wall
			levelRects.push( new TerrainRect( -200, 500, 800) );
			levelRects.push( new TerrainRect( -200, -200, 25, 700) );
			levelRects.push( new TerrainRect( 575, -200, 25, 700) );
			
			//Platforms
			levelRects.push( new TerrainRect( -50, 200, 75, 25) ); // Left side stairs
			levelRects.push( new TerrainRect( -25, 100, 75, 25) );
			levelRects.push( new TerrainRect( -50, 0, 75, 25) );
			levelRects.push( new TerrainRect( 100, 300, 100, 25) ); // Middle Steps
			levelRects.push( new TerrainRect( 300, 400, 100, 25) );
			
			levelRects.push( new TerrainRect( -175, 450, 50, 50) ); // Left side block
			
			draw();
		}
		
		//Perform collision with terrain rects. Returns Vector of collision rects that overlap.
		public function collideList( rect:RectBody ):Vector.<RectBody>
		{
			var out:Vector.<RectBody> = new Vector.<RectBody>();
			
			for (var i:int = 0; i < levelRects.length; i++)
			{
				var testRect : RectBody = levelRects[ i ];
				if ( rect.intersects( testRect ))
				{
					out.push(testRect);
				}
			}
			
			return out;
		}
		
		// Collides the rects against an x,y; returns the first rect it collides with. 
		public function collidePoint(x:Number, y:Number):TerrainRect
		{
			for (var i:int = 0; i < levelRects.length; i++)
			{
				if ( levelRects[i].contains( x, y ) )
				{
					return levelRects[i];
				}
			}
			return null;
		}
		
		public function draw():void
		{
			graphics.clear();
			
			for (var i:int = 0; i < levelRects.length; i++) {
				levelRects[i].draw(graphics);
			}
		}
		
	}

}