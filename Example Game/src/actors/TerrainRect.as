package actors {
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Rectangle;
	import interfaces.IDrawable;
	import physics.RectBody;
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 * 
	 * A collidable rectangle. Acts as a platform. Extends a geom rectangle for coordinates. Minimum size is 25x25. Must be multiples of 25.
	 * 
	 * Can collide against things, using rectangle intersection.
	 * 
	 * Does not support rotated rectangles yet.
	 */
	public class TerrainRect extends RectBody implements IDrawable {
		
		public var edgeColor:uint = 0x000000;
		public var fillColor:uint = 0x999999;
		
		private var display:Shape;
		
		public function TerrainRect(x:Number = 0, y:Number = 0, width:Number = 25, height:Number = 25) {
			
			width = Math.max(25, Math.floor(width / 25) * 25);
			height = Math.max(25, Math.floor(height / 25) * 25);
			
			display = new Shape();
			
			super(x, y, width, height);
			
			isFixed = true;
		}
		
		public function getCenterX():Number
		{
			return x + width / 2;
		}
		
		public function getCenterY():Number
		{
			return y + height / 2;
		}
		
		//Responsible for drawing the graphics API
		public function draw(targetGraphics:Graphics):void {
			targetGraphics.lineStyle(1, edgeColor);
			targetGraphics.beginFill(fillColor);
			targetGraphics.drawRect(x, y, width, height);
			targetGraphics.endFill();
		}
		
		public function getDisplay():Shape {
			return display;
		}
		
	}

}