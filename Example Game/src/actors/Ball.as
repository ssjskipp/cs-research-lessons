package actors
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author A.A. Grapsas
	 */
	public class Ball extends Sprite
	{
		[Embed(source="../../bin/images/ball.png")]
		private var embededImage : Class;
		
		public function Ball() 
		{
			setup();
		}
		
		private function setup() : void
		{
			var image : Bitmap = new embededImage();
			
			//Center the image
			image.x = -image.width / 2;
			image.y = -image.height / 2;
			
			this.addChild( image );
		}
	}
}