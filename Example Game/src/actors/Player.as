package actors {
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import physics.RectBody;
	
	import interfaces.ITickable;
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 */
	public class Player extends Sprite implements ITickable
	{
		[Embed(source="../../bin/images/ball.png")]
		private var embededImage : Class;
		
		// Keys mapped to their function
		public static const LEFT : uint = 65;
		public static const RIGHT : uint = 68;
		public static const JUMP : uint = 32;
		
		//Gravity constant
		private const GRAVITY : Number = 2.5;
		
		private var accelerate : Number = 2;
		
		public var halfHeight : Number;
		public var halfWidth : Number;
		
		private var canJump : Boolean;
		private var jumpDuration : int;
		
		private var physicsRect:RectBody;
		
		public function Player() {
			setup();
		}
		
		private function setup() : void
		{
			var image : Bitmap = new embededImage();
			
			//Calculate properties
			halfHeight = image.height / 2;
			halfWidth = image.width / 2;
			
			physicsRect = new RectBody(0, 0, image.width, image.height);
			
			//Jump only when feet touch the ground.
			canJump = false;
			
			this.addChild( image );
		}
		
		public function update(delta:Number):void
		{
			
			if ( Main.DownKeys[ LEFT ] )
			{
				physicsRect.velocity.x -= accelerate;
			}
			else if ( Main.DownKeys[ RIGHT ] )
			{
				physicsRect.velocity.x += accelerate;
			}
			else
			{
				if ( Math.abs(physicsRect.velocity.x) < 0.5 )
				{
					physicsRect.velocity.x *= 0.7;
				} else {
					physicsRect.velocity.x = 0;
				}
			}
			
			if ( Main.DownKeys[ JUMP ] )
			{
				if ( canJump )
				{
					physicsRect.velocity.y = -10;
					jumpDuration = 10;
					canJump = false;
				}
			}
			else if (!canJump)
			{
				jumpDuration = 0;
			}
			
			//Apply gravity, if not jumping
			if (jumpDuration <= 0)
			{
				physicsRect.velocity.y += accelerate;
			}
			else
			{
				jumpDuration--;
			}
			
			//Bound the velocities
			physicsRect.velocity.x = Math.min(10, Math.max(physicsRect.velocity.x, -10));
			physicsRect.velocity.y = Math.min(10, Math.max(physicsRect.velocity.y, -10));
			
			//Attempt to move the physics rect forward. Old (x, y) == (this.x, this.y)
			physicsRect.x += physicsRect.velocity.x;
			physicsRect.y += physicsRect.velocity.y;
			
			//Test the player against the world. Collision will prevent moving through. Up to you to respond.
			var outList : Vector.<RectBody> = Main.PTR.level.collideList( physicsRect );
			var isTouchingBottom : Boolean = false;
			for (var i:int = 0; i < outList.length; i++)
			{
				var out:RectBody = outList[i];
				
				//Find which side of out the physicsRect collided with first. Dirty, but works, thanks to the player being a square-ish
				var overlap : Rectangle = physicsRect.intersection( out );
				if (overlap.width > overlap.height)
				{
					physicsRect.velocity.y *= -0.3;
					
					//Top Colision
					if (y > out.y)
					{
						physicsRect.y = out.y + out.height;
					}
					//Bottom Colision
					else if (y < out.y)
					{
						isTouchingBottom = true;
						physicsRect.y = out.y - physicsRect.height;
					}
				}
				else
				{
					physicsRect.velocity.x *= -0.3;
					
					//Left Collision
					if (x > out.x)
					{
						physicsRect.x = out.x + out.width;
					}
					//Right Collision
					else if (x < out.x)
					{
						physicsRect.x = out.x - physicsRect.width;
					}
				}
			}
			
			if (isTouchingBottom)
			{
				canJump = true;
			}
			else
			{
				canJump = false;
			}
			
			//Move the player to the physics rect
			x = physicsRect.x;
			y = physicsRect.y;
			
		}
		
	}

}
