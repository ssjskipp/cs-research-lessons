package physics {
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 * 
	 * Rigid Body Rect, collides with other rects.
	 */
	public class RectBody extends Rectangle {
		
		private var center : Point;
		
		public var velocity : Point;
		
		public var mass : Number; // Default is 1
		
		public var isFixed : Boolean; // If the body is held fixed in space
		
		public function RectBody( x:Number = 0, y:Number = 0, width:Number = 25, height:Number = 25 )
		{
			super(x, y, width, height);
			
			setup();
		}
		
		private function setup():void
		{
			center = new Point( x + width / 2, y + height / 2 );
			
			velocity = new Point();
			
			isFixed = false;
			
			mass = 1;
		}
		
		public function setCenter(x:Number, y:Number):void
		{
			this.x = x - width / 2;
			this.y = y - height / 2;
		}
		
		public function getCenter():Point
		{
			center.x = x + width / 2;
			center.y = y + height / 2;
			return center;
		}
		
	}

}