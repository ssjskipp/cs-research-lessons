package interfaces
{
	
	/**
	 * ...
	 * @author Chris Federici
	 * 
	 * An interface describing what can be "ticked" -- anything that has an update function with intent to call each "frame"
	 */
	public interface ITickable {
		
		function update( delta:Number ):void;
		
	}
	
}