package interfaces {
	import flash.display.Graphics;
	
	/**
	 * ...
	 * @author Chris Federici (ssjskipp@gmail.com)
	 * 
	 * Something that draws itself with the graphics API.
	 */
	public interface IDrawable {
		
		function draw(targetGraphics:Graphics):void;
		
	}
	
}