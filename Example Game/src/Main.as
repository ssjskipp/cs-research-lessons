package
{
	import actors.Player;
	import actors.TerrainRect;
	import flash.automation.KeyboardAutomationAction;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	import interfaces.ITickable;
	import misc.Camera;
	import misc.GUI;
	import misc.Level;
	
	/**
	 * ...
	 * @author A.A. Grapsas and Chris Federici
	 */
	
	public class Main extends Sprite 
	{
		
		//A static variable means it is storred by the class, meaning Main.DownKeys is the full name of this variable
		public static var PTR : Main;
		
		public static var DownKeys : Dictionary; // A look up for key code to a value indicating if the key is down or not (boolean)
		
		public var gameDisplay : Sprite; // A sprite to hold all game content. This lets us layer GUI over it, and manipulate it.
		
		public var camera : Camera; // The camera is responsible for making sure the screen has the right content, scale, etc.
		
		public var gui : GUI; // This is the sprite that'll contain our Graphical User Interface (GUI) layer.
		
		public var player : Player; // The character you control
		
		public var level : Level; // The level to play on
		
		public var lastTick : Number; // The last time stamp that the game updated
		
		private var tickables : Vector.<ITickable>; // A vector is a list of objects. Handy for doing the same thing to a bunch of stuff.
		
		public function Main():void 
		{
			PTR = this;
			
			if ( stage ) init();
			else addEventListener( Event.ADDED_TO_STAGE, init );
		}
		
		private function init( e:Event = null ):void 
		{
			// entry point
			removeEventListener( Event.ADDED_TO_STAGE, init );
			
			// Initalize our local variables
			gameDisplay = new Sprite();
			
			camera = new Camera();
			camera.target = gameDisplay;
			camera.displayWidth = stage.stageWidth;
			camera.displayHeight = stage.stageHeight;
			
			gui = new GUI();
			
			level = new Level();
			
			player = new Player();
			
			gameDisplay.addChild( level );    // Add the level to the game display
			gameDisplay.addChild( player ); // Add the player over the level
			
			tickables = new Vector.<ITickable>();
			
			//Let's populate the tickables with everything we want to tick.
			tickables.push( player );
			tickables.push( gui );
			
			// Add the game display and GUI display
			addChild( gameDisplay );
			addChild( gui );
			
			// Create a new dictionary for tracking if the keys are pressed
			DownKeys = new Dictionary();
			
			// Setup listeners for when keys are pressed
			this.stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			this.stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			this.stage.addEventListener( Event.ENTER_FRAME, onEnterFrame );
		}
		
		private function onKeyDown( e:KeyboardEvent ):void
		{
			DownKeys[ e.keyCode ] = true;	// Indicate that this key is down (whatever the key is);
		}
		
		private function onKeyUp( e:KeyboardEvent ):void
		{
			DownKeys[ e.keyCode ] = false;	// Indicate that this key is no longer down
		}
		
		private function onEnterFrame( e:Event ):void
		{
			//Calculate the time difference from the last update
			var currentTime:Number = getTimer();
			var elapsedTime:Number = currentTime - lastTick;
			lastTick = currentTime;
			
			//Tick our tickables
			for ( var i : int = 0; i < tickables.length; i++ )
			{
				tickables[i].update( elapsedTime );
			}
			
			//Center the camera on the player, offset by half, after the player has moved
			camera.x = player.x + player.halfWidth;
			camera.y = player.y + player.halfHeight;
			
			//Tick the camera as the last operation
			camera.update( elapsedTime );
		}
		
	}
}